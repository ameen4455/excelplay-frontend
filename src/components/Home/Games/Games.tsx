import React from "react";
import "./Games.scss";

interface Props {
  type: string;
  rank?: number;
  status: string;
  logo: string;
  name: string;
  href?: string;
  index: number;
  isDisabled?: boolean;
}

const newCard = () => {
  return <div></div>;
};

const Games: React.FunctionComponent<Props> = (props) => {
  const rank = () => {
    if (props.type === "ranked") {
      return (
        <div className="rankDiv">
          <div className="rank">
            <p>RANK</p>
          </div>
          <div className="RankValue">{props.rank}</div>
        </div>
      );
    }
    // return <div className="PlayNow">{props.status}</div>;
  };

  return (
    <a style={{textDecoration: 'none'}} href={props.href}>
    <div className={"h-100 "}>
      <div
        className={`games position-relative ${
          props.isDisabled ? "disabled-game bg-5" : `bg-${props.index}`
        }`}
      >
          <div className="gameCell">
            <div className="Rankdetail">
              <div className={"title"}>{props.name}</div>
              {rank()}
            </div>
            <div className={"pic-box"}>
              <img src={props.logo} alt="dalabull" className="pic" />
            </div>
          </div>
          <div className={"play-button"}>{props.status}</div>
      </div>
    </div>
    </a>
  );
};

export default Games;
